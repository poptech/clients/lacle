/**
 * Created by romain on 23/04/2017.
 */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { unsetAuthBasic } from "../../actions/Auth";
import { FlatButton, FontIcon } from "material-ui";
import Settings from "../popup/Settings";
import { API_URL } from "../../constants/api";

const open = require("electron-open-url");

class Logged extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openSettings: false
    };
  }

  logOff() {
    this.props.AuthActions();
  }

  settings() {
    this.setState({ openSettings: true });
  }

  onProgress(e) {
    console.log(e);
  }

  downloadStat() {
    open({
      target: `${API_URL}/excel/export?start=2018-09-01&end=2019-07-01`,
      fallback: true
    });
  }

  closeSettingModal() {
    this.setState({ openSettings: false });
  }
  render() {
    return (
      <div>
        <FlatButton icon={<FontIcon className="material-icons">lock_open</FontIcon>} onTouchTap={this.logOff.bind(this)} />
        <FlatButton icon={<FontIcon className="material-icons">settings</FontIcon>} onTouchTap={this.settings.bind(this)} />
        <FlatButton icon={<FontIcon className="material-icons">vertical_align_bottom</FontIcon>} onTouchTap={this.downloadStat.bind(this)} />
        <Settings open={this.state.openSettings} closeFunc={this.closeSettingModal.bind(this)} />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    AuthActions: bindActionCreators(unsetAuthBasic, dispatch)
  };
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Logged);
