import {app, BrowserWindow, autoUpdater} from 'electron';
import installExtension, {REACT_DEVELOPER_TOOLS} from 'electron-devtools-installer';
import {enableLiveReload} from 'electron-compile';
const GhRelease = require('electron-gh-releases');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
if (process.platform !== "darwin") {
    let options = {
        repo: 'eksavoy/lacle',
        currentVersion: app.getVersion()
    };

    const updater = new GhRelease(options);

// Check for updates
// `status` returns true if there is a new update available
    updater.check((err, status) => {
        console.log(status);
        if (!err && status) {
            // Download the update
            updater.download()
        }else {
            console.log(err);
        }
    });

// When an update has been downloaded
    updater.on('update-downloaded', (info) => {
        // Restart the app and install the update
        console.log(info);
        updater.install()
    });

// Access electrons autoUpdater
    updater.autoUpdater
}


const isDevMode = process.execPath.match(/[\\/]electron/);

if (isDevMode) enableLiveReload({strategy: 'react-hmr'});

const createWindow = async () => {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
    });

    mainWindow.maximize();

    // and load the index.html of the app.
    mainWindow.loadURL(`file://${__dirname}/index.html`);

    // Open the DevTools.
    if (isDevMode) {
        await installExtension(REACT_DEVELOPER_TOOLS);
        mainWindow.webContents.openDevTools();
    }

    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow();
    }
});

app.commandLine.appendSwitch('--enable-touch-events');


// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
